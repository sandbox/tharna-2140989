<?php

function redirect_pathauto_defaults_drush_command() {
  $items = array();
  $items['redirect-pathauto-defaults'] = array(
    'description' => 'Create redirects from default pathauto patterns to overridden path aliases',
    'drupal dependencies' => array('redirect', 'pathauto'),
    'arguments' => array(
      'type' => 'Node type.'
    ),
    'aliases' => array('rpd'),
  );
  return $items;
}

function drush_redirect_pathauto_defaults($type) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->propertyCondition('type', $type);
  $result = $query->execute();
  $nids = array_keys($result['node']);
  foreach($nids as $nid){
    $node = node_load($nid);
    $internal_path = 'node/' . $node->nid;
    module_load_include('inc', 'pathauto', 'pathauto');
    $pathauto_alias = pathauto_create_alias('node', 'return', $internal_path, array('node' => $node), $node->type, $node->language);
    // get the current alias.
    $alias = drupal_get_path_alias($internal_path, $node->language);
    // Check if custom alias is set.
    if($alias != $pathauto_alias && !empty($pathauto_alias)) {
      // Add redirect from default pathauto alias.
      _redirect_pathauto_defaults_drush_add_redirect($pathauto_alias, $internal_path);
    }
  }
}


function _redirect_pathauto_defaults_drush_add_redirect($source, $dest, $code = 301) {
  // Create redirect object.
  $redirect = new stdClass();
  redirect_object_prepare($redirect);

  // Set the parameters.
  $redirect->source = $source;
  $redirect->redirect = $dest;
  $redirect->status_code = $code;

  // Do some sanity checks.
  // check that there there are no redirect loops
  if (url($redirect->source) == url($redirect->redirect)) {
    drush_set_error('redirect', t('You are attempting to redirect the page to itself. This will result in an infinite loop.'));
  }

  redirect_hash($redirect);
  if ($existing = redirect_load_by_hash($redirect->hash)) {
    if ($redirect->rid != $existing->rid) {
      drush_set_error('redirect', t('The source path %source is already being redirected to %redirect.', array('%source' => redirect_url($redirect->source, $redirect->source_options), '%redirect' => redirect_url($existing->redirect))));
    }
  }

  if (!drush_get_error()) {
    // Save the redirect if there were no errors.
    redirect_save($redirect);
  }
  drush_clear_error();
}
